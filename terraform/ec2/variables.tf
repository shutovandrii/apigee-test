variable "awsRegion" {}
variable "ami" {}
variable "regionName" {}
variable "vpcEnvAbbreviation" {}
variable "instanceType" {}
variable "hostsCount" {}
variable "cloudProvider" {}
variable "hostName" {}
variable "commonOwnerTag" {}
variable "timeZone" {}
variable "vpc_cidr" {}
variable "subnet_ids" {}
variable "vpc_id" {}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = "map"
  default     = {}
}

variable "servicesKeypair" {}
