resource "aws_instance" "mockserver" {
  count                       = "${var.hostsCount}"
  ami                         = "${var.ami}"
  instance_type               = "${var.instanceType}"
  key_name                    = "${var.servicesKeypair}"
  subnet_id = "${element(split(",", var.subnet_ids), 0)}"
  vpc_security_group_ids      = ["${aws_security_group.services.id}"]
  associate_public_ip_address = true
  user_data                   = "${file("user-data.sh")}"

  tags = {
    "Name"        = "${var.hostName}"
    "Environment" = "${var.vpcEnvAbbreviation}"
  }
}

output "private_ip" {
  value = "${aws_instance.mockserver.*.private_ip}"
}
