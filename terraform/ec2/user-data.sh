#!/usr/bin/env bash

timeZone = "UTC"
gitRepo = "https://shutovandrii@bitbucket.org/shutovandrii/apigee-test.git"
# Set Timezone
cat <<"__EOF__" > /etc/sysconfig/clock
ZONE="${timeZone}"
UTC=false
__EOF__

ln -sf /usr/share/zoneinfo/${timeZone} /etc/localtime

## Setup packages
sudo apt-get -y update && sudo apt-get -y upgrade
sudo apt-get install -y docker.io
sudo apt-get install npm -y

## Clone code for nodeJS
sudo git clone ${gitRepo}
cd apigee-test && sudo npm install
sudo cp ./express-swagger/apigee_test.service /etc/systemd/system
sudo systemctl start apigee_test
sudo systemctl enable apigee_test

## Start mongoDB container
sudo docker run -d --name mongo-app -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=mongoadmin -e MONGO_INITDB_ROOT_PASSWORD=secret mongo
