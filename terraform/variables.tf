variable "shared_cred_file" {
 default = "~/.aws/credentials"
}

variable "awsRegion" {
   default = "us-east-1"
}
variable "ami" {
   default = "ami-0ac019f4fcb7cb7e6"
}
variable "regionName" {
   default = "usa"
}
variable "vpcEnvAbbreviation" {
   default = "services"
}
variable "instanceType" {
   default = "t2.micro"
}
variable "hostsCount" {
   default = "1"
}
variable "cloudProvider" {
   default = "aws"
}
variable "hostName" {
   default = "mockserver"
}
variable "commonOwnerTag" {
   default = "shutovandrii@gmail.com"
}
variable "timeZone" {
  default = "UTC"
}
variable "vpc_cidr" {
  default = "10.0.0.0/24"
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = "map"
  default     = {}
}
variable "servicesKeypair" {
   default = "mockserver"
}
