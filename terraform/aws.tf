provider "aws" {
  region                  = "${var.awsRegion}"
  shared_credentials_file = "${var.shared_cred_file}"
  profile                 = "default"
}

module "network" {
  source         = "./network"
  name           = "${var.hostName}"
  vpc_cidr       = "${var.vpc_cidr}"
  
  vpcEnvAbbreviation      = "${var.vpcEnvAbbreviation}"

  tags = {
    Owner                 = "${var.commonOwnerTag}"
    Environment           = "${var.vpcEnvAbbreviation}"
  }
}

module "mockserver" {
  source = "./ec2"

  awsRegion               = "${var.awsRegion}"
  ami                     = "${var.ami}"
  regionName              = "${var.regionName}"
  vpcEnvAbbreviation      = "${var.vpcEnvAbbreviation}"
  instanceType            = "${var.instanceType}"
  hostsCount              = "${var.hostsCount}"
  hostName                = "${var.hostName}"
  commonOwnerTag          = "${var.commonOwnerTag}"
  cloudProvider           = "${var.cloudProvider}"
  timeZone                = "${var.timeZone}"
  subnet_ids              = "${module.network.subnet_ids}"
  vpc_cidr                = "${var.vpc_cidr}"
  vpc_id                  = "${module.network.vpc_id}"

  servicesKeypair         = "${var.servicesKeypair}"

  tags = {
    Owner                 = "${var.commonOwnerTag}"
    Environment           = "${var.vpcEnvAbbreviation}"
  }
}

output "subnet_ids" {
  value = "${module.network.subnet_ids}"
}
