variable "name" {
  default = "public"
}

resource "aws_internet_gateway" "public" {
  vpc_id = "${aws_vpc.services.id}"

  tags {
    Name = "${var.name}"
    Environment = "${var.vpcEnvAbbreviation}"
  }
}

resource "aws_subnet" "public" {
  vpc_id            = "${aws_vpc.services.id}"
  cidr_block        = "${var.vpc_cidr}"

  tags {
    Name = "${var.name}-public"
    Environment = "${var.vpcEnvAbbreviation}"
  }

  map_public_ip_on_launch = true
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.services.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.public.id}"
  }

  tags {
    Name = "${var.name}-public"
    Environment = "${var.vpcEnvAbbreviation}"
  }
}

resource "aws_route_table_association" "public" {
  count          = "${length(split(",", var.vpc_cidr))}"
  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public.id}"
}

output "subnet_ids" {
  value = "${join(",", aws_subnet.public.*.id)}"
}

output "igw_id" {
  value = "${aws_internet_gateway.public.id}"
}
