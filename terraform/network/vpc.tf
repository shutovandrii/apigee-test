variable "vpc_name" {
  default = "services"
}

resource "aws_vpc" "services" {
  cidr_block           = "${var.vpc_cidr}"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags {
    Name = "${var.vpc_name}"
  }
}

output "vpc_id" {
  value = "${aws_vpc.services.id}"
}

output "vpc_cidr" {
  value = "${aws_vpc.services.cidr_block}"
}
