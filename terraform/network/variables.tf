variable "vpc_cidr" {}
variable "vpcEnvAbbreviation" {}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = "map"
  default     = {}
}