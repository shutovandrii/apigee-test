var token = context.proxyRequest.headers['Authorization'];
var path = "/api/users"
var url = "http://ec2-54-234-167-1.compute-1.amazonaws.com:8001";
var req = new Request(url + path, "GET", {'Authorization' : token});
var usersObj = httpClient.send(req);

usersObj.waitForComplete();

if (!usersObj.isSuccess()) {
    context.proxyResponse.content.asJSON.error = 'Error contacting mockserver web service';
} else {
    print(usersObj.getResponse().content);
    context.proxyResponse.content = usersObj.getResponse().content;
}