//Parse the original response into a JSON object
var reqPath = context.targetRequest.path;
var pathVars = reqPath.split("/")

// Prepare user object definition data response. 
var user             = { id: pathVars[3],
                          name: "Karlos",
                          email: "sample1@mail.com",
                          birthDate: "2016-11-09 10:30",
                          address: {
                              id: 1,
                              street: "First str.",
                              state: "WD",
                              city: "Madrid",
                              country: "Ukraine",
                              zip: "01001"
                          }};

//Set the response variable. 
context.proxyResponse.status = 200;
context.proxyResponse.content = JSON.stringify(user);