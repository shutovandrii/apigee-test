'use strict';

var cacheApigee = require('volos-cache-apigee');
var cacheMemory = require('volos-cache-memory');
var constants = require('./constants');
var Promise = require('bluebird');

var cacheOptions = {
  fallback: cacheMemory,
  ttl: constants.CACHE_DEFAULT_TTL
};

var cache = cacheApigee.create(constants.CACHE_NAME, cacheOptions);

var cacheFace = {
    get: function(cacheKey, count) {
        var _this = this;
        if (count === undefined) {
            count = 0;
        }

        return new Promise(function(resolve, reject) {
            cache.get(cacheKey, function(err, c) {
                if (err) {
                    // retry if cache retrival fails, possible known issue on Apigee
                    // https://community.apigee.com/questions/56732/intermittent-cache-issue-in-nodejs-proxy.html
                    if (err.message.includes('Invalid cached data') && count < 3) {
                        ++count;
                        resolve(_this.get(cacheKey, count));
                    } else {
                        reject(err);
                    }
                } else if (c) {
                    resolve(c);
                } else {
                    reject(constants.CACHE_ITEM_NOT_FOUND);
                }
            });
        });
    },
    set: function(cacheKey, value, options) {
        return new Promise(function(resolve, reject) {
            cache.set(cacheKey, value, options, function(err, c) {
                if (err) {
                    reject(err);
                }
                resolve(c);
            });
        });
    },
    delete: function(cacheKey) {
        return new Promise(function(resolve, reject) {
            cache.delete(cacheKey, function(err, c) {
                if (err) {
                    reject(err);
                }
                resolve(c);
            });
        });
    }
};

module.exports = cacheFace;
