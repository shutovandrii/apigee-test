'use strict';

var contentQuota = require('./content.controller');


module.exports = {
    getMap: {
        'Users_getUsers': contentQuota.checkContentQuota,
        'Products_getProducts': contentQuota.checkContentQuota,
        'Cities_getCities': contentQuota.checkContentQuota
    }
};
