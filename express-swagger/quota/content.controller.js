'use strict';

const constants = require('../utils/constants');
const userService = require('../service/UsersService');

module.exports.checkContentQuota = function (req, res, next) {
    var uuid = req.decodedToken.uuid;
    var user = userService.findUserById(uuid)
        .then(result => {
            if (result.length > 0) {
                const currentTime = new Date().getTime();
                const timeDiff = currentTime - result[0].count_requests.timestamp;
                if (timeDiff > constants.QUOTA_TIME) {
                    result[0].count_requests.number_requests = 0;
                    result[0].count_requests.timestamp = currentTime;
                    userService.updateUser(uuid, result[0])
                        .then(result => {
                            next();
                        });
                } else if (result[0].count_requests.number_requests < constants.QUOTA) {
                    result[0].count_requests.number_requests++;
                    userService.updateUser(uuid, result[0])
                        .then(result => {
                            next();
                        });
                } else {
                    res.status(403).send({message: 'You have exceed your quota limit of ' + constants.QUOTA + ' requests per minute.'});
                }
            } else {
                res.status(403).send({message: 'You are not allowed to access this request by quota limit.'});
            }
        });
};
