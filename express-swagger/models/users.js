'use strict';

const mongoose = require('mongoose');

const usersSchema = new mongoose.Schema({
  uuid: String,
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  count_requests: {
      number_requests: Number,
      timestamp: Number
  },
});

module.exports = mongoose.model('Users', usersSchema);
