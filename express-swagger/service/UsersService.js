'use strict';

const uuid = require('uuid/v1');
const TokenVerifyService = require('../utils/tokenVerify.service');
const UsersModel = require('../models/users');
const mongoose = require('mongoose');
const constants = require('../utils/constants');
var Promise = require('bluebird');

/**
 * user authentication
 * User authentication endpoint 
 *
 * body User  (optional)
 * returns inline_response_200
 **/
exports.auth = function(body) {
  let params = body;
  let userCred = null;

  return this.findUserByEmail(params.email)
    .then(user => {
        if (user.length == 0 || user[0].password !== params.password) {
            let err = {
                code: '401',
                message: 'Not registered user'
            };
            throw err;
        } else {
            user[0].token = TokenVerifyService.generateToken(user[0].uuid);
        }
        userCred = user[0];
  
        return {
            code: 200,
            message: "OK",
            uuid: userCred.uuid,
            token: userCred.token
        };
    });
};


/**
 * list of users
 * Get list of all users 
 *
 * returns List
 **/
exports.getUsers = function() {
    mongoose.connect(constants.mongo);
    let users = [];
    return UsersModel.find()
        .then(doc => {
          mongoose.disconnect();
          let users = [];
          doc.forEach(element => {
              const user = {
                  uuid: element.uuid,
                  email: element.email
              };
              users.push(user);
          });
          return users;
        });
};


/**
 * User register
 * Create new user 
 *
 * body User  (optional)
 * returns User
 **/
exports.registerUser = function(body) {
  const params = body;
  const _this = this;
  let userCred = {};

  return this.findUserByEmail(params.email)
    .then(result => {
        if (result.length > 0) {
            userCred.uuid = result[0].uuid;
            userCred.token = TokenVerifyService.generateToken(result[0].uuid);
            return userCred;
        } else {
            return _this.saveUser(params)
                .then(saved => {
                    userCred.uuid = saved.uuid;
                    userCred.token = TokenVerifyService.generateToken(saved.uuid);
                    return userCred;
                });
        }
    });
};

exports.saveUser = function(required) {
    mongoose.connect(constants.mongo);
    var user = {
        uuid: uuid(),
        email: required.email,
        password: required.password,
        count_requests: {
          number_requests: 0,
          timestamp: new Date().getTime()
        }
    };
    var userModel = new UsersModel(user);

    return userModel.save()
        .then(doc => {
          mongoose.disconnect();
          return doc;
        });
};

exports.findUserByEmail = function(email) {
    mongoose.connect(constants.mongo);
    return UsersModel.find({email: email})
        .then(doc => {
          mongoose.disconnect();
          return doc;
        });
};

exports.findUserById = function(uuid) {
    mongoose.connect(constants.mongo);
    return UsersModel.find({uuid: uuid})
        .then(doc => {
          mongoose.disconnect();
          return doc;
        });
};

exports.updateUser = function(uuid, body) {
    mongoose.connect(constants.mongo);
    return UsersModel.update({ uuid: uuid }, { $set: body}).exec()
        .then(doc => {
          mongoose.disconnect();
          return doc;
        });
  }
  