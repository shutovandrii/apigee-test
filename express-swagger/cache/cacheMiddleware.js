'use strict';

/**
 * @param {Object} req the express request object
 * @param {Object} res the express response object
 * @param {Function} next go the next middleware
 * @returns {undefined} Returns nothing
 */
module.exports.initMiddleware = function (req, res, next) {
    var oldWrite = res.write,
    oldEnd = res.end;

    var chunks = [];

    res.write = function (data) {
        chunks.push(new Buffer(data));

        oldWrite.apply(res, arguments);
    };

    res.end = function (data) {
        if (data) {
            chunks.push(new Buffer(data));
        }

        var body = Buffer.concat(chunks).toString('utf8');
        res.body = body;

        oldEnd.apply(res, arguments);
    };
    next();
};
