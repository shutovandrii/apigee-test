'use strict';

var constants = require('../utils/constants');
var errors = require('../utils/errors');

module.exports.putInCache = function (cacheKey, cacheOptions, req, res, next) {
    res.on('finish', function() {
        var cache = req.app.get('cache');
        setResponseCodeAndBody(cache, cacheKey, res, cacheOptions);
    });
    next();
};

module.exports.sendResponseFromCacheIfExists = function (cacheKey, req, res, next) {
    var cache = req.app.get('cache');
    getResponseCodeAndBody(cache, cacheKey)
        .then(function(cachedItem) {
            res.status(cachedItem.statusCode).send(cachedItem.body);
        })
        .catch(function() {
            next();
        });
};

/**
 * get a response from the cache
 * @param {Object} cache the cache that is being used
 * @param {String} cacheKey the key of the cached item
 * @returns {Promise<Object>} Returns a promise of a cached response object
 */
function getResponseCodeAndBody(cache, cacheKey) {
    return cache.get(cacheKey).then(function(result) {
        return JSON.parse(result);
    });
}

/**
 * put a response code and body in the cache
 * @param {Object} cache the cache that is being used
 * @param {String} cacheKey the key of the cached item
 * @param {String} response the response that must be cached
 * @param {String} options the cacheoptions, if null: default ttl is set
 * @returns {Promise<Object>} Returns a promise
 */
function setResponseCodeAndBody(cache, cacheKey, response, options) {
    var cacheOptions = options || {'ttl': constants.CACHE_DEFAULT_TTL};
    var resString = JSON.stringify(
        {
            statusCode: response.statusCode,
            body: JSON.parse(response.body)
        });

    return cache.set(cacheKey, resString, cacheOptions);
}

module.exports.purgeCacheIfExists = function (cacheKey, req, res) {
    var cache = req.app.get('cache');
    cache.delete(cacheKey)
        .then(function() {
            res.status(204).send();
        })
        .catch(function (err) {
            errors.sendResponseError(res, err);
        });
};
