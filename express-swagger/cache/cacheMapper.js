'use strict';

var contentCache = require('./content.controller');


module.exports = {
    getMap: {
        'Users_getUsers': contentCache.getContentFromCache,
        'Products_getProducts': contentCache.getContentFromCache,
        'Cities_getCities': contentCache.getContentFromCache
    },
    putMap: {
        'Users_getUsers': contentCache.putContentInCache,
        'Products_getProducts': contentCache.putContentInCache,
        'Cities_getCities': contentCache.putContentInCache
    }
};
