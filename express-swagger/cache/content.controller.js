'use strict';

var constants = require('../utils/constants');
var cacheHelper = require('./cacheHelper');

module.exports.getContentFromCache = function (req, res, next) {
    var cacheKey = prepareContentCacheKey(req);
    cacheHelper.sendResponseFromCacheIfExists(cacheKey, req, res, next);
};

module.exports.putContentInCache = function (req, res, next) {
    var cacheKey = prepareContentCacheKey(req);
    cacheHelper.putInCache(cacheKey, null, req, res, next);
};

module.exports.purgeContentFromCache = function (req, res) {
    var cacheKey = prepareContentCacheKey(req);
    cacheHelper.purgeCacheIfExists(cacheKey, req, res);
};

/**
 * Prepare cache key.
 * @param {Object} req the express request object
 * @returns {String} Returns cache key
 */
function prepareContentCacheKey (req) {
    return req.swagger.apiPath.replace(new RegExp('/api/', 'g'), '');
}
